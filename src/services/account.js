const knex = require('../db/connection');
const messageConfig = require('../config/msgConfig');

function getAllAccount() {
    return knex('sms_account')
        .select(['sms_account.*', 'sms_employee.image as empimage', 'sms_employee.name as empname', 'sms_employee.nric as empnric',
            'sms_employee.dob as empdob', 'sms_employee.position_id as empposition',
            'sms_employee.department_id as empdepartment', 'sms_employee.start_date as empstart',
            'sms_employee.prefixphone as empprefix', 'sms_employee.parmanent_address as empparmanent',
            'sms_employee.temporary_address as emptemporary', 'sms_employee.father_name as empfather',
            'sms_employee.mother_name as empmother', 'sms_employee.education as empedu',
            'sms_employee.social_media_link as empsocial', 'sms_position.name as posname', 'sms_department.name as depname'])
        .leftJoin('sms_employee', 'sms_account.employee_id', 'sms_employee.id')
        .leftJoin('sms_position', 'sms_account.position_id', 'sms_position.id')
        .leftJoin('sms_department', 'sms_account.department_id', 'sms_department.id')
    // .select('*');
    //.select(['sms_employee.*','sms_position.Position_Name as posname'])
    //.leftJoin('sms_position', 'sms_employee.position', 'sms_position.id')

}

async function getUserAccount(id) {
    const model = await knex('sms_account')
        .select(['sms_account.*', 'sms_employee.image as empimage', 'sms_employee.name as empname',
            'sms_employee.nric as empnric', 'sms_employee.dob as empdob', 'sms_employee.position_id as empposition',
            'sms_employee.department_id as empdepartment', 'sms_employee.start_date as empstart',
            'sms_employee.prefixphone as empprefix', 'sms_employee.parmanent_address as empparmanent', 'sms_employee.temporary_address as emptemporary',
            'sms_employee.father_name as empfather', 'sms_employee.mother_name as empmother', 'sms_employee.education as empedu',
            'sms_employee.social_media_link as empsocial', 'sms_position.name as posname', 'sms_department.name as depname'])
        .leftJoin('sms_employee', 'sms_account.employee_id', 'sms_employee.id')
        .leftJoin('sms_position', 'sms_account.position_id', 'sms_position.id')
        .leftJoin('sms_department', 'sms_account.department_id', 'sms_department.id')
        .where({ 'sms_account.id': parseInt(id) });
    return model[0]
    // return knex('sms_account')
    //     .select([
    //         'sms_employee.*'
    //     ])
    //     //.select(['sms_employee.*','sms_position.Position_Name as posname'])
    //     .leftJoin('sms_employee', 'sms_employee.code', 'sms_account.ecode')
    //     .where(
    //         {
    //             'sms_account.id': parseInt(id)
    //         }
    //     )
}

async function getAccountById(id) {
    const model = await knex('sms_account')
        .select('*')
        .where({ id: parseInt(id) });
    return model[0]
}

async function addAccount(account) {

    return knex.transaction((trx) => {
        return knex('sms_account')
            .insert(account)
            .transacting(trx)
            .then((response) => {
                console.log('Response is ' + JSON.stringify(response));
                if (response[0] > 0) {
                    return response[0];
                } else {
                    throw ('error');
                }
            })
            .then(trx.commit)
            .catch((err) => {
                trx.rollback;
                console.error('Exception error....', err);
                return 'error'
            });
    })
        .then((response) => {
            console.log(response)
            console.log('Transaction object return object: ', response);
            if (response && response == 'error') {
                return 'error';
            } else {
                return getAccountById(response);
            }
        })
        .catch((err) => {
            console.error(err);
        });
}

async function updateAccount(id, account) {
    return knex.transaction(async (trx) => {
        return knex('sms_account')
            .transacting(trx)
            .update(account)
            .where({ id: parseInt(id) })
            .then((response) => {
                console.log('Response is ' + JSON.stringify(response));
                if (response > 0) {
                    return 'success';
                } else {
                    return 'error';
                }
            })
            .then(trx.commit)
            .catch((err) => {
                trx.rollback;
                console.error('Exception error....' + err);
                return 'error'
            });
    })
        .then((response) => {
            console.log('Transaction object return object: ' + response);
            if (response && response == 'success') {
                return getAccountById(id);
            } else {
                return 'error';
            }
        })
        .catch((err) => {
            console.error(err);
        });
}

async function deleteAccount(id) {
    return knex.transaction(async (trx) => {
        return knex('sms_account')
            .transacting(trx)
            .del()
            .where({ id: parseInt(id) })
            .then((response) => {
                if (response) {
                    return 'success';
                } else {
                    return 'error';
                }
            })
            .then(trx.commit)
            .catch((err) => {
                trx.rollback;
                return 'error'
            });
    })
        .then((response) => {
            if (response && response == 'success') {
                return 'success';
            } else {
                return 'error';
            }
        })
        .catch((err) => {
            // console.error(err);
        });
}

module.exports = {
    getAllAccount,
    getAccountById,
    addAccount,
    updateAccount,
    deleteAccount,
    getUserAccount
};