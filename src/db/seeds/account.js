const acc = [

  {
    id: 1,
    name: 'Smith',
    role: 'Admin',
    ecode: 'Ecode-123',
    nirc: '12/SRT(N)123',
    email: 'smith@gmail.com',
    phone_number: '09123',
    password: 'smith',
    con_pas: 'smith',
    created_by: 'super-admin',
    updated_by: 'super-admin'
  },
  {
    id: 2,
    name: 'John',
    role: 'Service Man',
    ecode: 'Ecode-125',
    nirc: '12/SRT(N)129',
    email: 'john@gmail.com',
    phone_number: '09122',
    password: 'john',
    con_pas: 'john',
    created_by: 'super-admin',
    updated_by: 'super-admin'
  }
];
var roles = [
  {
    id: 1,
    name: 'Admin',
    description: 'Super Admin Role',
    created_by: 'super-admin',
    updated_by: 'super-admin'
  },
  {
    id: 2,
    name: 'Service Man',
    description: 'Admin Role',
    created_by: 'super-admin',
    updated_by: 'super-admin'
  },
];


exports.seed = function (knex, Promise) {
  return knex('sms_account').del()
    .then(function () {
      return knex('sms_account').insert(acc);
    })
    .then(() => {
      return knex('sms_role').insert(roles);
    })
};
