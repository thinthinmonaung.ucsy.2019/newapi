const position = [
  {
    id: 1,
    name: 'Service',
    description: 'To serve the customer service',
    created_by: 'super-admin',
    updated_by: 'super-admin'
  },
  {
    id: 2,
    name: 'Accountant',
    description: 'To calculate the finance and account',
    created_by: 'super-admin',
    updated_by: 'super-admin'
  },



];
exports.seed = function (knex, Promise) {
  // Deletes ALL existing entries
  return knex('sms_position').del()
    .then(function () {
      // Inserts seed entries
      return knex('sms_position').insert(position);
    });
};