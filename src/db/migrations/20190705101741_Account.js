

exports.up = function (knex, Promise) {
    return Promise.all([
        knex.schema.createTable('sms_account', (t) => {
            t.increments('id').primary();
            t.integer('employee_id').unsigned();
            t.integer('position_id').unsigned();
            t.integer('department_id').unsigned();
            t.foreign('employee_id').references('id').inTable('sms_employee');
            t.foreign('position_id').references('id').inTable('sms_position');
            t.foreign('department_id').references('id').inTable('sms_department');;
            t.string('name').notNullable();
            t.string('role').notNullable();
            t.string('ecode').notNullable().unique();
            t.string('nirc').notNullable().unique();
            t.string('email').notNullable().unique();
            t.string('phone_number').notNullable().unique();
            t.string('password').notNullable();
            t.string('con_pas').notNullable();

            t.timestamp('created_at').defaultTo(knex.fn.now())
            t.string('created_by').notNullable();
            t.timestamp('updated_at').defaultTo(knex.fn.now());
            t.string('updated_by').notNullable();
            t.integer('version_no').defaultTo(1);
            t.boolean('status').defaultTo(true);
            t.decimal('sort_order_no').defaultTo();
        }),
        knex.schema.createTable('sms_role', (t) => {
            t.increments();
            t.string('name').notNullable().unique();
            t.string('description');
            t.timestamp('created_at').defaultTo(knex.fn.now());
            t.string('created_by').notNullable();
            t.timestamp('updated_at').defaultTo(knex.fn.now());
            t.string('updated_by').notNullable();
            t.integer('version_no').defaultTo(1);
            t.boolean('status').defaultTo(true);
            t.decimal('sort_order_no').defaultTo(0);
        }),
    ]);

};

exports.down = function (knex, Promise) {
    return Promise.all([
        knex.schema.dropTable('sms_account'),
        knex.schema.dropTable('sms_role'),
    ]);


};
