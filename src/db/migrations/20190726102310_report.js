
exports.up = function(knex, Promise) {
    return Promise.all([
        knex.schema.createTable('sms_report', (t) => {
          t.increments();
          t.string('job_code').notNullable();  
          
          t.string('fup_number').notNullable();  
          t.string('date').notNullable().unique();
          t.string('wkh').notNullable().unique();
          t.string('wd').notNullable().unique();
          t.string('remark').notNullable();
         
          //t.foreign('position').references('id').inTable('sms_position').onDelete('CASCADE');;
          
          // Table Default Fields
          t.timestamp('created_at').defaultTo(knex.fn.now());  
          t.string('created_by').notNullable();
          t.timestamp('updated_at').defaultTo(knex.fn.now());
          t.string('updated_by').notNullable();
          t.integer('version_no').defaultTo(1);
          t.boolean('status').defaultTo(true);
          t.decimal('sort_order_no').defaultTo(0);      
        }), 

    ]);    

};

exports.down = function(knex, Promise) {
  return Promise.all([
      knex.schema.dropTable('sms_report'),
  ]);
};