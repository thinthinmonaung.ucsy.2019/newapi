const Router = require('koa-router');
const accQueries = require('../services/account');
const messageConfig = require('../config/msgConfig');

const router = new Router();
const BASE = '/account';

router.get(BASE, async (ctx) => {
    try {

        const account = await accQueries.getAllAccount();

        ctx.body = {
            status: messageConfig.status.success,
            data: account
        };
    } catch (err) {
        ctx.status = 400;
        ctx.body = {
            status: messageConfig.status.error,
            message: err.message || messageConfig.message.error.internalError
        };
    }
});


router.get(`${BASE}/user/:id`, async (ctx) => {
    try {

        const account = await accQueries.getUserAccount(ctx.params.id);

        ctx.body = {
            status: messageConfig.status.success,
            data: account
        };
    } catch (err) {
        ctx.status = 400;
        ctx.body = {
            status: messageConfig.status.error,
            message: err.message || messageConfig.message.error.internalError
        };
    }
});

router.get(`${BASE}/:id`, async (ctx) => {
    try {
        const account = await accQueries.getAccountById(ctx.params.id);
        ctx.body = {
            status: messageConfig.status.success,
            data: account
        };
    } catch (err) {
        ctx.status = 400;
        ctx.body = {
            status: messageConfig.status.error,
            message: err.message || messageConfig.message.error.internalError
        };
    }
});

router.post(`${BASE}`, async (ctx) => {
    try {
        const account = await accQueries.addAccount(ctx.request.body);
        console.log(ctx.request.body)
        if (account && account != 'error') {
            ctx.status = 201;
            ctx.body = {
                status: messageConfig.status.success,
                data: account
            };
        } else {
            ctx.status = 404;
            ctx.body = {
                status: messageConfig.status.error,
                message: messageConfig.message.error.internalError
            };
        }
    } catch (err) {
        ctx.status = 400;
        ctx.body = {
            status: messageConfig.status.error,
            message: err.message || messageConfig.message.error.internalError
        };
    }
});

router.put(`${BASE}/:id`, async (ctx) => {
    try {
        const account = await accQueries.updateAccount(ctx.params.id, ctx.request.body);
        if (account && account != 'error') {
            ctx.status = 200;
            ctx.body = {
                status: 'success',
                data: account
            };
        } else {
            ctx.status = 404;
            ctx.body = {
                status: messageConfig.status.error,
                message: messageConfig.message.error.internalError
            };
        }
    } catch (err) {
        // console.log('Router Exception Error .... : ' + err);        
        ctx.status = 400;
        ctx.body = {
            status: messageConfig.status.error,
            message: err.message || messageConfig.message.error.internalError
        };
    }
});

router.delete(`${BASE}/:id`, async (ctx) => {

    try {
        const account = await accQueries.deleteAccount(ctx.params.id);
        console.log(account)
        if (account && account != 'error') {
            ctx.status = 200;
            ctx.body = {
                status: 'success',
                data: account
            };
        } else {
            ctx.status = 404;
            ctx.body = {
                status: messageConfig.status.error,
                message: messageConfig.message.error.fileNotFound
            };
        }
    } catch (err) {
        // console.log('Router Exception Error .... : ' + err);        
        ctx.status = 400;
        ctx.body = {
            status: messageConfig.status.error,
            message: err.message || messageConfig.message.error.internalError
        };
    }
});

module.exports = router;